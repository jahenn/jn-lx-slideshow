var xElements = new Array();
var xElemenstUsing = new Array();
var XElementPrev = new Array();
var xAnimate = new Array();
var xAnimatePrev = new Array();
var xinfo = new Array();
$.fn.slideShow = function(params){

	var flag = false;
	var urlParam = params.dataSource;
	var xThis = $(this);
	$.getJSON(urlParam, function(data){
	}).done(function(data){
		var thisID = $(xThis).attr("id");

		xElemenstUsing[thisID] = 0;
		xElements[thisID] = new Array();
		XElementPrev[thisID] = new Array();
		xAnimate[thisID] = true;
		xinfo[thisID] = $('<div id="null"></div>');
		/** Creacion de estructura **/
		$(xThis).css({
			"width" : "100%",
			"height" : "200px",
			"background" : "rgb(242,242,242)",
			"position" : "relative",
			"overfloat" : "hidden"
		});

		var ulMaster = $('<ul class="sElements"></ul>');

		var punteroArray = 0;
		var mainLeft = 40;

		var maxThisID = 0;
		
		$.each(data.response.data,function(key,val){
			var img = new Image();
			img.src = val.src;

			img.width = val.width -10;
			img.height = 200;

			xElements[thisID][punteroArray] =  $('<li></li>');
			var imgContainer = $('<div style="width: ' + (val.width - 5) + 'px"></div>');
			imgContainer.append(img);
			xElements[thisID][punteroArray].append(imgContainer);
			ulMaster.append(xElements[thisID][punteroArray]);
			mainLeft += (parseFloat(val.width) + 5);
			punteroArray += 1;

			maxThisID ++;


			$(img).click(function(e){
				xleft = e.pageX;
				xTop = e.pageY;
				xAnimatePrev = xAnimate[thisID];
				xAnimate[thisID] = false;

				var content = $('<div class="contenidoExtra" ></div>');
				var pointer = $('<div class="pointer" style="position: absolute; top: -30px; width: 30px; height: 30px;"><img src="http://ns1.maximas.com.mx/test/images/arrow.png"/></div>');
				var closeIMG = $('<div class="closeIMG"><img src="http://ns1.maximas.com.mx/test/images/close.png" /></div>')
				pointer.css("left", xleft + "px");

				$.get(val.url, function(data){
				}).done(function(data){
					content.html(data);
					content.append(pointer);
					content.append(closeIMG);
					$(xThis).animate({
						height : "200px"
					},500,function(){
						$(xinfo[thisID]).remove();
						$(xThis).append(content);
						xinfo[thisID] = content;
						var xHeight = $(xinfo[thisID]).height();
						$(xinfo[thisID]).css("top","10px");
						$(xinfo[thisID]).css("height","0px");
						$(content).animate({
							height : xHeight + "px"
						},1000);
						$(xThis).animate({
							height : xHeight + 210 + "px"
						},1000,function(){
							$("body, html").animate({
								scrollTop : xTop
							});
						});
					});
				});
				$(closeIMG).click(function(){
					xAnimate[thisID] = xAnimatePrev;
					$(xThis).animate({
						height: 200 + "px"
					},500,function(){
						$(xinfo[thisID]).remove();
					});
				});
			});

		});
		mainLeft = mainLeft -10;



		var masterContainer = $('<div class="MasterContainer"></div>');
		var container = $('<div class="sContainer" style="width: ' + mainLeft + 'px"></div>');
		var prevButton = $('<div class="prevButton"><img src="https://bitbucket.org/jahenn/jn-lx-slideshow/raw/91af1e38086531290ccb66e2fb289bae37138cd2/img/arrowLeft.png"></div>');
		var nextButton = $('<div class="nextButton"><img src="https://bitbucket.org/jahenn/jn-lx-slideshow/raw/91af1e38086531290ccb66e2fb289bae37138cd2/img/arrowRight.png"></div>');



		$(xThis).append(prevButton);
		$(xThis).append(nextButton);
		
		container.append(ulMaster);
		container.appendTo(masterContainer);
		masterContainer.appendTo($(xThis));
		//$(this).replaceWith(masterContainer);




		nextButton.click(function(){
			XElementPrev[thisID][xElemenstUsing[thisID]] = masterContainer.scrollLeft();
			masterContainer.animate({
				scrollLeft : + parseFloat(xElements[thisID][xElemenstUsing[thisID]].css("width").replace("px","")) + $(masterContainer).scrollLeft()
			},500);

			//alert(maxThisID);

			if(xElemenstUsing[thisID] >= (maxThisID-1))
			{
				$(masterContainer).animate({
					scrollLeft: 0
				},500);
				xElemenstUsing[thisID] = 0;
			}else{
				xElemenstUsing[thisID] += 1;
			}
		});

		prevButton.click(function(){
			if(xElemenstUsing[thisID] == 0)
			{
				return;
			}else
			{
				xElemenstUsing[thisID] -= 1;
				masterContainer.animate({
					scrollLeft: + XElementPrev[thisID][xElemenstUsing[thisID]]
				});
			}
		});

		nextButton.mouseenter(function(){
			xAnimatePrev[thisID] = xAnimate[thisID];
			xAnimate[thisID] = false;
		}).mouseleave(function(){
			xAnimate[thisID] = xAnimatePrev[thisID];
		});

		timer(nextButton,thisID);

	}).error(function(data){
		alert(data[0]);
	});

	return;
}

function timer(object, thisID)
	{
		setTimeout(function() {
			if(xAnimate[thisID] == true)
			{
				object.click();
			}
			timer(object,thisID);
		}, 3000);
	}